<?php

// languages availables
$app['languages'] = array('es','ca');

// header content
$app['header'] = array(
    'es' => array(
        array(
            'title'     =>  'Title slider 1',
            'subtitle'  =>  'subtitle slider 1',
            'button'    =>  'Button 1',
        ),
        array(
            'title'     =>  'Title slider 2',
            'subtitle'  =>  'subtitle slider 2',
            'button'    =>  'Button 2',
        ),
        array(
            'title'     =>  'Title slider 3',
            'subtitle'  =>  'subtitle slider 3',
            'button'    =>  'Button 3',
        ),
    ),
    'ca' => array(
        array(
            'title'     =>  'CA Title slider 1',
            'subtitle'  =>  'CA subtitle slider 1',
            'button'    =>  'CA Button 1',
        ),
        array(
            'title'     =>  'CA Title slider 1',
            'subtitle'  =>  'CA subtitle slider 1',
            'button'    =>  'CA Button 1',
        ),
        array(
            'title'     =>  'CA Title slider 1',
            'subtitle'  =>  'CA subtitle slider 1',
            'button'    =>  'CA Button 1',
        ),
    ),
);

// team content
$app['team'] = array(
    'es' => array(
        'intro' =>  ' años ofreciendo nuestros servicios',
        'text' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut eaque, laboriosam veritatis, quos non quis ad perspiciatis, totam corporis ea, alias ut unde.',
        'employees' =>  array(
            array(
                'id'    =>  1,
                'img'   =>  'team-men-3.jpg',
                'name'  =>  'Dr. Manuel Marcoss',
                'jobtitle'  =>  'Socio fundador',
                'cv' => array(
                    'text'  =>  'Número colegiado: 5463',
                    'education' => array(
                        '1# 1999-2004 - LICENCIATURA EN ODONTOLOGÍA',
                        '1# 2006-2009 - MÁSTER D´ORTODONCIA(UAB)',
                    ),
                    'experience' => array(
                        '1# 12004-2005 -Clínica Odontológica del Hospital Universitario de Montpellier',
                        '1# 2003 -Clínica Dental del Dr. Lorenzo Mulet Sastre - Valencia',
                        '1# 2006 -Clínica Dental de la Dra. Norma Schwenzera – Frankfurt',
                    ),
                ),
            ),
            array(
                'id'    =>  2,
                'img'   =>  'team-women-2.jpg',
                'name'  =>  'Maika Marcos',
                'jobtitle'  =>  'Directora médica i ortodoncia',
                'cv' => array(
                    'text'  =>  '',
                    'education' => array(
                        '2# 1999-2004 - LICENCIATURA EN ODONTOLOGÍA',
                    ),
                    'experience' => array(
                        '2# 2004-2005 - Clínica Odontológica del Hospital Universitario de Montpellier',
                        '2# 2003 - Clínica Dental del Dr. Lorenzo Mulet Sastre - Valencia',
                    ),
                ),
            ),
            array(
                'id'    =>  3,
                'img'   =>  'team-men-1.jpg',
                'name'  =>  'Vicenç Esteller',
                'jobtitle'  =>  'Cirurgía implants',
                'cv' => array(
                    'text'  =>  'Número colegiado: 5463',
                    'education' => array(
                        '3# 1999-2004 - LICENCIATURA EN ODONTOLOGÍA',
                        '3# 2006-2009 - MÁSTER D´ORTODONCIA(UAB)',
                    ),
                    'experience' => array(
                        '3# 2004-2005 -Clínica Odontológica del Hospital Universitario de Montpellier',
                        '3# 2003 -Clínica Dental del Dr. Lorenzo Mulet Sastre - Valencia',
                        '3# 2006 -Clínica Dental de la Dra. Norma Schwenzera – Frankfurt',
                    ),
                ),
            ),
            array(
                'id'    =>  4,
                'img'   =>  'team-women-1.jpg',
                'name'  =>  'Carmen Calvo',
                'jobtitle'  =>  'Endodoncia',
                'cv' => array(
                        'text'  =>  'Número colegiado: 5463',
                    'education' => array(
                        '4# 1999-2004 - LICENCIATURA EN ODONTOLOGÍA',
                        '4# 2006-2009 - MÁSTER D´ORTODONCIA(UAB)',
                    ),
                    'experience' => array(
                        '4# 2004-2005 -Clínica Odontológica del Hospital Universitario de Montpellier',
                        '4# 2003 -Clínica Dental del Dr. Lorenzo Mulet Sastre - Valencia',
                        '4# 2006 -Clínica Dental de la Dra. Norma Schwenzera – Frankfurt',
                    ),
                ),
            ),
            array(
                'id'    =>  5,
                'img'   =>  'team-women-3.jpg',
                'name'  =>  'M.Sol García',
                'jobtitle'  =>  'Protesis i general',
                'cv' => array(
                    'text'  =>  'Número colegiado: 5463',
                    'education' => array(),
                    'experience' => array(),
                ),
            ),
            array(
                'id'    =>  6,
                'img'   =>  'team-women-4.jpg',
                'name'  =>  'Emi Leon',
                'jobtitle'  =>  'Higienes',
                'cv' => array(
                    'text'  =>  'Número colegiado: 5463',
                    'education' => array(
                        '6# 1999-2004 - LICENCIATURA EN ODONTOLOGÍA',
                        '6# 2006-2009 - MÁSTER D´ORTODONCIA(UAB)',
                    ),
                    'experience' => array(
                        '6# 2004-2005 -Clínica Odontológica del Hospital Universitario de Montpellier',
                        '6# 2003 -Clínica Dental del Dr. Lorenzo Mulet Sastre - Valencia',
                        '6# 2006 -Clínica Dental de la Dra. Norma Schwenzera – Frankfurt',
                    ),
                ),
            ),
            array(
                'id'    =>  7,
                'img'   =>  'team-women-5.jpg',
                'name'  =>  'Laura Gama',
                'jobtitle'  =>  'Auxiliar',
                'cv' => array(
                    'text'  =>  'Número colegiado: 5463',
                    'education' => array(
                        '7# 1999-2004 - LICENCIATURA EN ODONTOLOGÍA',
                        '7# 2006-2009 - MÁSTER D´ORTODONCIA(UAB)',
                    ),
                    'experience' => array(
                        '7# 2004-2005 -Clínica Odontológica del Hospital Universitario de Montpellier',
                        '7# 2003 -Clínica Dental del Dr. Lorenzo Mulet Sastre - Valencia',
                        '7# 2006 -Clínica Dental de la Dra. Norma Schwenzera – Frankfurt',
                    ),
                ),
            ),
            array(
                'id'    =>  8,
                'img'   =>  'team-women-6.jpg',
                'name'  =>  'Mireia Hernández',
                'jobtitle'  =>  'Recepción',
                'cv' => array(
                    'education' => array(),
                    'experience' => array(),
                ),
            ),
        ),
    ),
    'ca' => array(
        'intro' =>  ' anys oferint els nostres serveis',
        'text' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut eaque, laboriosam veritatis, quos non quis ad perspiciatis, totam corporis ea, alias ut unde.',
        'employees' =>  array(
            array(
                'id'    =>  1,
                'img'   =>  'team-men-3.jpg',
                'name'  =>  'Dr. Manuel Marcos',
                'jobtitle'  =>  'Soci fundador',
                'cv' => array(
                    'text'  =>  'Número colegiado: 5463',
                    'education' => array(
                        '1# 1999-2004 - LICENCIATURA EN ODONTOLOGÍA',
                        '1# 2006-2009 - MÁSTER D´ORTODONCIA(UAB)',
                    ),
                    'experience' => array(
                        '1# 12004-2005 -Clínica Odontológica del Hospital Universitario de Montpellier',
                        '1# 2003 -Clínica Dental del Dr. Lorenzo Mulet Sastre - Valencia',
                        '1# 2006 -Clínica Dental de la Dra. Norma Schwenzera – Frankfurt',
                    ),
                ),
            ),
            array(
                'id'    =>  2,
                'img'   =>  'team-women-2.jpg',
                'name'  =>  'Maika Marcos',
                'jobtitle'  =>  'Directora médica i ortodoncia',
                'cv' => array(
                    'text'  =>  'Número colegiado: 5463',
                    'education' => array(
                        '2# 1999-2004 - LICENCIATURA EN ODONTOLOGÍA',
                    ),
                    'experience' => array(
                        '2# 2004-2005 - Clínica Odontológica del Hospital Universitario de Montpellier',
                        '2# 2003 - Clínica Dental del Dr. Lorenzo Mulet Sastre - Valencia',
                    ),
                ),
            ),
            array(
                'id'    =>  3,
                'img'   =>  'team-men-1.jpg',
                'name'  =>  'Vicenç Esteller',
                'jobtitle'  =>  'Cirurgía implants',
                'cv' => array(
                    'text'  =>  'Número colegiado: 5463',
                    'education' => array(
                        '3# 1999-2004 - LICENCIATURA EN ODONTOLOGÍA',
                        '3# 2006-2009 - MÁSTER D´ORTODONCIA(UAB)',
                    ),
                    'experience' => array(
                        '3# 2004-2005 -Clínica Odontológica del Hospital Universitario de Montpellier',
                        '3# 2003 -Clínica Dental del Dr. Lorenzo Mulet Sastre - Valencia',
                        '3# 2006 -Clínica Dental de la Dra. Norma Schwenzera – Frankfurt',
                    ),
                ),
            ),
            array(
                'id'    =>  4,
                'img'   =>  'team-women-1.jpg',
                'name'  =>  'Carmen Calvo',
                'jobtitle'  =>  'Endodoncia',
                'cv' => array(
                    'text'  =>  'Número colegiado: 5463',
                    'education' => array(
                        '4# 1999-2004 - LICENCIATURA EN ODONTOLOGÍA',
                        '4# 2006-2009 - MÁSTER D´ORTODONCIA(UAB)',
                    ),
                    'experience' => array(
                        '4# 2004-2005 -Clínica Odontológica del Hospital Universitario de Montpellier',
                        '4# 2003 -Clínica Dental del Dr. Lorenzo Mulet Sastre - Valencia',
                        '4# 2006 -Clínica Dental de la Dra. Norma Schwenzera – Frankfurt',
                    ),
                ),
            ),
            array(
                'id'    =>  5,
                'img'   =>  'team-women-3.jpg',
                'name'  =>  'M.Sol García',
                'jobtitle'  =>  'Protesis i general',
                'cv' => array(
                    'text'  =>  'Número colegiado: 5463',
                    'education' => array(),
                    'experience' => array(),
                ),
            ),
            array(
                'id'    =>  6,
                'img'   =>  'team-women-4.jpg',
                'name'  =>  'Emi Leon',
                'jobtitle'  =>  'Higienes',
                'cv' => array(
                    'text'  =>  'Número colegiado: 5463',
                    'education' => array(
                        '6# 1999-2004 - LICENCIATURA EN ODONTOLOGÍA',
                        '6# 2006-2009 - MÁSTER D´ORTODONCIA(UAB)',
                    ),
                    'experience' => array(
                        '6# 2004-2005 -Clínica Odontológica del Hospital Universitario de Montpellier',
                        '6# 2003 -Clínica Dental del Dr. Lorenzo Mulet Sastre - Valencia',
                        '6# 2006 -Clínica Dental de la Dra. Norma Schwenzera – Frankfurt',
                    ),
                ),
            ),
            array(
                'id'    =>  7,
                'img'   =>  'team-women-5.jpg',
                'name'  =>  'Laura Gama',
                'jobtitle'  =>  'Auxiliar',
                'cv' => array(
                    'text'  =>  'Número colegiado: 5463',
                    'education' => array(
                        '7# 1999-2004 - LICENCIATURA EN ODONTOLOGÍA',
                        '7# 2006-2009 - MÁSTER D´ORTODONCIA(UAB)',
                    ),
                    'experience' => array(
                        '7# 2004-2005 -Clínica Odontológica del Hospital Universitario de Montpellier',
                        '7# 2003 -Clínica Dental del Dr. Lorenzo Mulet Sastre - Valencia',
                        '7# 2006 -Clínica Dental de la Dra. Norma Schwenzera – Frankfurt',
                    ),
                ),
            ),
            array(
                'id'    =>  8,
                'img'   =>  'team-women-6.jpg',
                'name'  =>  'Mireia Hernández',
                'jobtitle'  =>  'Recepción',
                'cv' => array(
                    'text'  =>  'Número colegiado: 5463',
                    'education' => array(),
                    'experience' => array(),
                ),
            ),
        ),
    ),
);

// services content
$app['services'] = array(
    'es' => array(
        'intro' => 'ES :: Texto intro servicios',
        'services' => array(
            'conservative' => array(
                'title_slider' => 'conservadora',
                'title' => 'odontología conservadora',
                'content' => array(
                    'La odontología conservadora es el área de la Odontología destinada a reemplazar los elementos dentarios ausentes y a mantener las piezas presentes en la boca.',
                    'Antes de la ejecución de un tratamiento dental es necesario efectuar una adecuada planificación del caso a tratar mediante los recursos diagnósticos existentes en nuestra Clínica (Radiografías panorámicas, Tomografías, Modelos de estudios, articulación de modelos, encerado diagnóstico, etc...) para luego determinar el tratamiento ideal para cada paciente.',
                ),
            ),
            'preventive' => array(
                'title_slider' => 'preventiva',
                'title' => 'odontología preventiva',
                'content' => array(
                    'La prevención es el mejor tratamiento para evitar todo tipo de enfermedades, sobretodo patologías bucodentales.',
                    'Este es un concepto que antiguamente solo se aplicaba en relación a los niños, pero que actualmente engloba a todos nuestros pacientes.',
                    'La caries y los problemas periodontales (de encías) son las patologías dentales más habituales en nuestra sociedad. Los problemas periodontales son los que trata el Periodoncista y las higienistas dentales. Una correcta y oportuna terapia de mantenimiento periodontal (luego de realizar la higiene y los raspados, si fuesen necesarios) le evitará constantes problemas periodontales, producidos por realizar tratamientos esporádicos, sin el seguimiento y control correspondiente.',
                    'Mediante un seguimiento personalizado de los pacientes, nuestro personal se pondrá en contacto con usted y le recordará la necesidad de hacer las revisiones periódicas. Esto nos permitirá mantener su boca en perfecto estado de salud e higiene.',
                    'El enfoque de este campo de la odontología es para evitar la enfermedad, y si está instaurada, utilizar tratamientos poco invasivos, que retrasen o eviten tratamientos más agresivos (extracción y posterior reposición de piezas dentales).',
                )
            ),
            'aesthetics' => array(
                'title_slider' => 'estética',
                'title' => 'estética dental',
                'content' => array(
                    'En nuestra sociedad, la estética juega cada día un papel más importante. Los dientes ya no sólo sirven para masticar, para pronunciar… sino que además nos influyen en las relaciones con los demás. Tener una bonita sonrisa nos hace ganar en confianza y seguridad, y nos permite sonreír abiertamente y sin reparos.',
                    'Existen distintos tratamientos que, aisladamente o combinados, permiten conseguir el objetivo buscado: tener una óptima sonrisa; así por ejemplo:',
                    'Con las carillas y coronas dentales podemos mejorar el aspecto de los dientes en cuanto a forma, proporciones, tamaño, color...',
                    'Con el blanqueamiento dental conseguimos un color, una tonalidad y una luminosidad en la sonrisa óptimos.',
                    'Con la ortodoncia conseguiremos colocar los dientes en su posición adecuada mejorando no sólo la estética de los mismos, sino también los problemas funcionales.',
                    'Con las intervenciones quirúrgicas en las encías, gingivectomías, podemos retocar su contorno para conseguir una uniformidad y unas proporciones adecuadas.',
                ),
            ),
            'implantology' => array(
                'title_slider' => 'implantología',
                'title' => 'implantología',
                'content' => array(
                    '<strong>¿Qué son los implantes?</strong>',
                    'Son sustitutos de las raíces de los dientes cuando éstos han sido extraídos. Son, en realidad, tornillos de un material especial biocompatible (titanio) que se roscan en el hueso para sujetar, posteriormente, el diente o la muela que falta.',
                    '<strong>¿Por qué tiene tanta importancia reponer los dientes perdidos?</strong>',
                    'La reposición de las piezas que faltan en la boca es vital para mantener una correcta salud bucodental. Probablemente, si a todo el mundo que le falta algún diente fuese consciente de las anomalías, problemas y trastornos que ello desencadena, seguro que cuidaría más su dentadura y valoraría más el hecho de no extraer con tanta alegría las piezas dentales.',
                    'Por ello la premisa fundamental es INTENTAR SIEMPRE NO EXTRAER NINGUNA PIEZA DENTAL. Hoy en día la Odontología moderna dispone de métodos y técnicas que pueden conservar cualquier diente siempre que la raíz esté sana.',
                    'La falta de una pieza dental supone el deterioro progresivo del resto de las piezas',
                    'Sin embargo, si ya falta esa muela en la boca, los trastornos que ello desencadena se van estableciendo a lo largo de los años. Y esto es, precisamente, el gran problema, ya que el paciente no es consciente de lo que está sucediendo en su boca y los trastornos se establecen perjudicialmente de forma lenta y silenciosa resumiéndose en los siguientes puntos:',
                    '1. Movimiento de los dientes que rodean el espacio que deja la muela extraída. Las muelas que contactaban con la muela ausente se van extruyendo y las adyacentes se van tumbando hacia el espacio vacío. Ello añade la pérdida funcional de más muelas y también maloclusiones y trastornos temporomandibulares.',
                    '2. También provoca reabsorciones y pérdida de retención ósea en las muelas vecinas y en el propio espacio que deja la muela donde el proceso alveolar se va atrofiando progresivamente.',
                    '3. Igualmente, el paciente comienza a masticar por el otro lado desencadenando una abrasión dental exagerada en la zona de carga así como una alteración unilateral de la articulación temporomandibular.',
                    '4. Cuando la falta de piezas es más numerosa, la patología se multiplica y, sobretodo, los trastornos temporomandibulares, ya que la articulación precisa el soporte de los dientes para estar sana.',
                    '<strong>¿Qué ventajas tienen los implantes sobre otro tipo de prótesis?</strong>',
                    '1 Se mantiene y conserva el volumen óseo. Si no hay algo dentro del hueso, éste se va atrofiando progresivamente.',
                    '2 No hay que tallar los dientes adyacentes para sujetar la prótesis',
                    '3 La duración y el pronóstico de la prótesis no va a depender de las muelas que la sujetan. El implante es independiente y su éxito no está sujeto a la salud futura de las piezas adyacentes.',
                    '4 La sensación propioceptiva es más fisiológica. El paciente siente con más naturalidad un implante que un puente fijo.',
                ),
            ),
            'orthodontics' => array(
                'title_slider' => 'ortodoncia',
                'title' => 'ortodoncia',
                'content' => array(
                    'La ortodoncia es la especialidad de la odontología que se ocupa del diagnóstico, prevención y tratamiento de las malposiciones dentarias y de las deformaciones de los maxilares.',
                    'La corrección de los dientes y de las bases óseas se realiza mediante una serie de aparatos que pueden ser fijos o removibles, dependiendo de cada caso. Estos aparatos de ortodoncia y las técnicas de trabajo han evolucionado mucho, de manera que hoy en día disponemos de técnicas estéticas que han hecho que los aparatos prácticamente no se aprecien.',
                    'En nuestra clínica los tratamientos de ortodoncia están dirigidos a pacientes de todas las edades y el tratamiento es realizado por profesionales especializados y en colaboración con diversos profesionales de otras áreas para realizar a cada paciente un tratamiento interdisciplinario. A los niños se aconseja llevarlos al ortodoncista antes de los 6 años para poder detectar posibles alteraciones dentales o esqueléticas importantes de manera precoz y poder corregirlas.',
                    'Los doctores realizaran un exhaustivo examen clínico y un estudio mediante la toma de una serie de registros como: modelos dentales, fotografías faciales e intraorales y radiografías que les permitirán realizar un adecuado y personalizado plan de tratamiento.',
                    '<strong>La ortodoncia invisible</strong>',
                    'Consiste en una serie de alineadores prácticamente invisibles y extraíbles que se cambian progresivamente por uno nuevo. Cada uno de los alineadores se fabrica específicamente para tus diente. Conforme vayas cambiando de alineador, tus dientes se irán moviendo, poco a poco, semanas tras semana, hasta que estén alineados y en la posición final prescrita por el odontólogo.',
                    'Es la mejor manera de transformar tu sonrisa sin alterar tu vida cotidiana. Es removible ( se pueden extraer para comer, beber, cepillarse los dientes, etc.), cómodo (no hay brackets ni alambres que puedan irritar tus encías o el interior de tu boca) y está hecho a medida.',
                    '<strong>Ortodoncia fija</strong>',
                    'La ortodoncia fija es aquella que no puede ser retirada por el paciente.',
                    'Comúnmente es conocida como brackets, refiriéndose a las piezas, generalmente metálicas, que sujetas a los dientes sostienen el arco que produce el desplazamiento dental.',
                    'En los últimos años se han producido grandes avances en el campo de la ortodoncia fija existiendo una amplia gama adaptada en función cada uno de los casos y de las necesidades estéticas del paciente.',
                    '<strong>Ortodoncia de colores</strong>',
                    'Pensada especialmente para los más pequeños de la casa para los que llevar aparato puede generar problemas estéticos.',
                    'Se trata de una ortodoncia fija metálica cuyas ligaduras se pueden personalizar con sus colores favoritos.',
                    'La mejor forma de evitar que rechacen llevar brackets haciendo que se convierta en un juego',
                    '<strong>Ortodoncia autoligable</strong>',
                    'La ortodoncia de autoligado elimina las ligaduras de sujeción del arco al bracket, facilitando la limpieza y evitando la retención de restos de alimentos.',
                    'Además, el diseño de los brackets puede ayudar a que los dientes consigan la alineación adecuada más rápidamente, acortando así el tiempo de tratamiento.',
                    'Si además te preocupa la estética, cuentas también con la Ortodoncia Autoligable Cerámica, brackets del color de tus dientes para que sonreír no suponga un reto.',
                    '<strong>Ortodoncia fija cerámica</strong>',
                    'La ortodoncia cerámica es un tipo de aparatología fija en la que los brackets son fabricados en material cerámico adaptándose a la tonalidad del propio diente.',
                    'Al ser colocados sobre las piezas dentales, los brackets apenas son visibles proporcionándonos una estética extraordinaria.',
                    '<strong>Ortodoncia removible</strong>',
                    'La ortodoncia removible va anclada a los dientes pudiéndose quitar sin ayuda del odontólogo.',
                    'Indicada en determinado tipo de casos como movimientos dentales limitados o para modificar el crecimiento, puede ser retirada para comer o para su limpieza.',
                    'En muchas ocasiones la aparatología removible es usada como apoyo a la fija después de un tratamiento con ortodoncia fija o brackets.',
                ),
            ),
            'periodontics' => array(
                'title_slider' => 'peridoncia',
                'title' => 'peridoncia',
                'content' => array(
                    '<strong>¿Qué son las enfermedades periodontales?</strong>',
                    'Las enfermedades periodontales son un conjunto de enfermedades que afectan a los tejidos (periodonto) que rodean y sujetan a los dientes en los maxilares.',
                    'Son enfermedades de naturaleza inflamatoria y de causa infecciosa (causadas por bacterias) que dependiendo de su grado de afectación las denominamos gingivitis, cuando el proceso inflamatorio solamente afecta al periodonto superficial (la encía) y no están afectados los tejidos que sujetan el diente. Cuando el proceso inflamatorio afecta a los tejidos periodontales profundos, se produce destrucción del hueso y ligamento que soportan y sujetan los dientes. A este grado de afectación lo denominamos periodontitis. Si la periodontitis no se trata, ésta evoluciona y destruye todo el soporte del diente y con ello se produce la pérdida del diente.',
                    '<strong>Periodontitis</strong>',
                    'Las enfermedades periodontales son las enfermedades más extendidas del ser humano, incluye la gingivitis que consiste en una infección situada debajo de la encía, generalmente indolora que inflama la encía, hace que sangre fácilmente y si no se trata en alglinos individuos progresa a periodontitis.',
                    'La periodontitis, antiguamente llamada piorrea, destruye el hueso y el ligamento que soporta los dientes, provocando movilidad y separación de los dientes y si no se trata, pérdida de los mismos. Las enfermedades periodontales se pueden prevenir o en su defecto tratar con gran éxito, pudiendo así conservar los dientes toda nuestra vida.',
                    '<strong>¿Cuáles son los signos de la enfermedad periodontal?</strong>',
                    'Estos procesos inflamatorios raramente cursan un dolor intenso, por lo que muchos pacientes los padecen sin ser conscientes de ello. Los primeros signos y síntomas que aparecen en las fases más tempranas de la enfermedad (gingivitis) son el enrojecimiento e hinchazón de la encía, sangrado de las encías al cepillarse los dientes o de un modo espontáneo. Cuando las enfermedades periodontales han evolucionado a periodontitis, suelen aparecer otros síntomas tales como:',
                    '1. Retracción de las encías, sensación de dientes más largos.',
                    '2. Movilidad de los dientes. ',
                    '3. Separación de los dientes. ',
                    '4. Aumento de la sensibilidad dentaria, sobre todo al frío. ',
                    '5. Sensación de quemazón y dolor de encías. ',
                    '6. Mal aliento. ',
                    '7. Aparición de abscesos y flemones en la encía.',
                    '<strong>¿Cuáles son las causas de las enfermedades periodontales?</strong>',
                    'Se trata de enfermedades de causa infecciosa bacteriana. Se debe a unas bacterias que todos tenemos en la boca, alrededor de los dientes, y que si no eliminamos correctamente, ganan acceso al espacio entre el diente y la encía y pueden colonizar este espacio creciendo en número y provocando la reacción inflamatoria y destructiva que define a estas enfermedades. Las bacterias por si mismas no son capaces de provocar las consecuencias de estas enfermedades, sino que necesitan de un individuo susceptible (predisposición genética) y un medio ambiente adecuado (factores como tabaco y stress son factores de riesgo muy importantes en la colonización de estas bacterias).',
                    '<strong>¿Son las enfermedades periodontales muy frecuentes?</strong>',
                    'La gingivitis es una de las enfermedades más frecuentes del ser humano, siendo muy frecuente en todas las edades ( en jóvenes 75% de 20 a 25 años ).',
                    'Una parte de las gingivitis, aún en ausencia de tratamiento, no progresan a la destrucción de los tejidos periodontales de soporte dentario ( periodontitis ). La periodontitis es muy rara en jóvenes y adolescentes (aproximadamente 1 caso/10.000). Sin embargo su frecuencia aumenta con la edad (alrededor del 10% a los 30-40 años y del 25-30% entre 50 y 60 años). Por lo tanto, es una enfermedad',
                    '<strong>¿Desde que aparecen estos signos hasta que empieza a destruirse el hueso pasa mucho tiempo?</strong>',
                    'Depende, unos pacientes evolucionan rápido y otros lentamente. Los de evolución rápida acostumbran a ser jóvenes y pueden perder sus dientes en 5 años. En otros casos la evolución puede ser más lenta, pero si no se tratan adecuadamente puede llegar a perder también sus dientes.',
                    '<strong>¿Este tipo de infección que provoca la enfermedad periodontal puede afectar al estado general del paciente?</strong>',
                    'Si el paciente está en buen estado de salud, normalmente no se afecta su estado general, por ser una infección localizada exclusivamente en la boca. ',
                    'Sin embargo, puede afectar muy negativamente en casos de pacientes con enfermedades sistémicas crónicas tales como diabetes, enfermedades cardíacas, circulatorias, etc. Igualmente, está demostrada una clara asociación entre periodontitis y riesgo de infarto de miocardio y riesgo de niños prematuros y de bajo peso en mujeres embarazadas. Estas infecciones también pueden representar un grave riesgo a pacientes inmunodeprimidos (pacientes con defensas bajas, tal como ocurre en pacientes en tratamiento de cáncer, pacientes con SIDA, etc.).',
                    '<strong>¿La periodontitis o piorrea se cura?</strong>',
                    'Sí. El objetivo del tratamiento es eliminar la infección que produce la enfermedad. Una vez controlada la infección, el proceso destructivo del hueso se detiene y de este modo se pueden conservar los dientes y los tejidos periodontales sanos. Dependiendo de la cantidad de hueso de soporte destruído, los dientes una vez tratados tendrán mejor o peor pronóstico desde un punto de vista funcional. Por ello es importante un tratamiento precoz. ',
                    '<strong>¿Ha cambiado actualmente el antiguo concepto de que era?</strong>',
                    'Totalmente, no solamente el ser humano ha prolongado mucho sus años de vida, sino que también ha mejorado la calidad de vida de las personas de mayor edad.',
                    'Uno de los factores más importantes de esta mejor calidad de vida es el mantenimiento de la dentición en buenas condiciones de salud y función. Para ello es clave una adecuada prevención y tratamiento de las enfermedades periodontales. Con ello se podrán sobrepasar los 70 años con relativa facilidad, no sólo en cuanto a salud general, sino también al mantenimiento de una buena dentición.',
                ),
            ),
            'prosthesis' => array(
                'title_slider' => 'prótesis',
                'title' => 'prótesis dental',
                'content' => array(
                    'Una prótesis dental es un elemento artificial destinado para restaurar la anatomía de una o varias piezas dentarias restaurando también la relación entre los maxilares, a la vez que devuelve la dimensión vertical, y repone los dientes.',
                    'Los objetivos más importantes de las prótesis dentales son: la funcionalidad, la retención, el soporte y la estabilidad.',
                    'Hay diferentes tipos de prótesis dentales:',
                    'Removibles: son aquellas que el paciente puede retirar de la boca y volver a colocar por sí mismo.',
                    'No removibles: son aquellas que bien por cementado o atornillado, sólo puede colocar y retirar el odontólogo.',
                    'Prótesis mixta: Constan de una parte que va fija en la boca y otra que el paciente puede retirar y colocar.',
                    'Prótesis sobre implantes o implantosoportadas: Son aquellas que están soportadas por implantes fijos. Pueden estar fijas a los mismos, o ser un dispositivo mixto que pueda retirarse de los implantes con facilidad.',
                    '<strong>Uso de la prótesis, cuidados e higiene:</strong>',
                    'Cada prótesis presentará unas determinadas indicaciones de las que deberá ser informado el paciente por el odontólogo. Según el tipo de prótesis el paciente deberá concienciarse en mayor o menor medida del mantenimiento de la misma, no obstante, son las prótesis removibles (aquellas que el paciente puede retirar de la boca y colocar nuevamente por sí mismo), las que necesitarán de una especial indicación para el usuario. Algunos consejos generales para este tipo de prótesis bien podrían ser:',
                    'Lavar la prótesis después de cada comida, usando un cepillo de dientes convencional o bien un cepillo especial para prótesis (que se puede adquirir en cualquier farmacia), aplicando jabón después aclarar muy bien con agua. El uso de pasta dentífrica para limpiar este tipo de prótesis está contraindicado ya que provoca la opacidad y deterioro de la capa externa de la prótesis eliminando con el tiempo su brillo original, promoviendo la porosidad del material, que en el futuro facilitará la agregación de bacterias a la misma y consigo la formación de placa bacteriana y sarro.',
                    'Es importante la higiene de la prótesis así como de la propia boca, principalmente por salud y por tanto por comodidad y bienestar, evitando además el deterioro de la prótesis, así como malos olores o la simple pérdida de las características estéticas de la misma. Por ello el odontólogo suele insistir al paciente en la necesidad de realizar esta operación, si no tras cada comida, al menos una vez al día.',
                    'Retirar la prótesis para dormir, puesto que es recomendable que los tejidos de la boca descansen de la presión a la que se ven sometidas, al menos unas horas diariamente.',
                    'Mientras se mantenga la prótesis fuera de la boca, se aconseja conservarla en agua, bien sola, o bien añadiéndole unas pastillas desinfectantes preparadas para ese menester. De este modo se evitan golpes y deformaciones, al mismo tiempo que se conservan mejor los materiales de los que están hechas las prótesis.',
                    'Cuando se mantenga la prótesis retirada de la boca, tal vez antes de dormir, es aconsejable realizar masajes en las encías, puesto que de este modo se mejorará el riego sanguíneo, lo que hará que la reabsorción de los tejidos sea al menos algo menor.',
                    'En coronas y puentes fijos, el cepillado deberá realizarse de igual modo que el de una dentadura natural, existiendo hilos dentales especialmente diseñados para este tipo de prótesis, siendo recomendables también los enjuagues bucales con clorhexidina previamente recetados por el odontólogo.',
                    '<strong>Revisiones e incidencias:</strong>',
                    'Siempre que aparezcan ulceraciones, dolor o inestabilidad de la prótesis, deberá acudir de inmediato a la consulta del dentista.',
                    'Se debe realizar una revisión cada seis meses para observar el estado de los dientes y mucosas, para detectar desgastes en los dientes y realizar las adaptaciones apropiadas para corregir los desajustes provocados por el paso del tiempo.',
                ),
            ),
            'dentistry' => array(
                'title_slider' => 'odontopediatría',
                'title' => 'odontopediatría',
                'content' => array(
                    'La Odontopediatría es la rama de la Odontología que se dedica, en forma especial, a la atención de pacientes infantiles y adolescentes. Una de las medidas más importantes para mantener la salud bucal, es la visita temprana y regular al odontopediatra, quien podrá diagnosticar cualquier alteración que se haya producido. En el caso de los niños, esto cobra aún mayor importancia, debido a que las patologías que afectan a los dientes temporales o de leche, tienen una progresión más rápida que en los adultos.',
                    'Es por eso que, aconsejamos que las primeras visitas al odontopediatra coincidan con la aparición de los primeros dientes de leche, alrededor de los 6 meses de edad. Desde este momento, el odontopediatra podrá controlar el desarrollo de la dentición y el estado de la piezas dentarias, identificar alguna patología o alteración que requiera una atención prematura, enseñar las debidas y correctas pautas de higiene, tanto para el bebé como para los padres, contribuir a adoptar patrones alimenticios saludables y comenzar a instaurar procedimientos preventivos para evitar la aparición de caries y sus consecuencias.',
                    'Además, la visita temprana al odontopediatra, permitirá realizar procedimientos sencillos y agradables para los niños, contribuyendo de esta manera a crear un ambiente de seguridad y confianza, que se traducirá en pacientes adultos con mejor salud bucal y mayor disposición a la atención odontológica.',
                ),
            ),
        ),
    ),
    'ca' => array(
        'intro' => 'CA :: Texto intro servicios',
        'services' => array(
            'conservative' => array(
                'title_slider' => 'conservadora',
                'title' => 'odontología conservadora',
                'content' => array(
                    'La odontología conservadora es el área de la Odontología destinada a reemplazar los elementos dentarios ausentes y a mantener las piezas presentes en la boca.',
                    'Antes de la ejecución de un tratamiento dental es necesario efectuar una adecuada planificación del caso a tratar mediante los recursos diagnósticos existentes en nuestra Clínica (Radiografías panorámicas, Tomografías, Modelos de estudios, articulación de modelos, encerado diagnóstico, etc...) para luego determinar el tratamiento ideal para cada paciente.',
                ),
            ),
            'preventive' => array(
                'title_slider' => 'preventiva',
                'title' => 'odontología preventiva',
                'content' => array(
                    'La prevención es el mejor tratamiento para evitar todo tipo de enfermedades, sobretodo patologías bucodentales.',
                    'Este es un concepto que antiguamente solo se aplicaba en relación a los niños, pero que actualmente engloba a todos nuestros pacientes.',
                    'La caries y los problemas periodontales (de encías) son las patologías dentales más habituales en nuestra sociedad. Los problemas periodontales son los que trata el Periodoncista y las higienistas dentales. Una correcta y oportuna terapia de mantenimiento periodontal (luego de realizar la higiene y los raspados, si fuesen necesarios) le evitará constantes problemas periodontales, producidos por realizar tratamientos esporádicos, sin el seguimiento y control correspondiente.',
                    'Mediante un seguimiento personalizado de los pacientes, nuestro personal se pondrá en contacto con usted y le recordará la necesidad de hacer las revisiones periódicas. Esto nos permitirá mantener su boca en perfecto estado de salud e higiene.',
                    'El enfoque de este campo de la odontología es para evitar la enfermedad, y si está instaurada, utilizar tratamientos poco invasivos, que retrasen o eviten tratamientos más agresivos (extracción y posterior reposición de piezas dentales).',
                )
            ),
            'aesthetics' => array(
                'title_slider' => 'estética',
                'title' => 'estética dental',
                'content' => array(
                    'En nuestra sociedad, la estética juega cada día un papel más importante. Los dientes ya no sólo sirven para masticar, para pronunciar… sino que además nos influyen en las relaciones con los demás. Tener una bonita sonrisa nos hace ganar en confianza y seguridad, y nos permite sonreír abiertamente y sin reparos.',
                    'Existen distintos tratamientos que, aisladamente o combinados, permiten conseguir el objetivo buscado: tener una óptima sonrisa; así por ejemplo:',
                    'Con las carillas y coronas dentales podemos mejorar el aspecto de los dientes en cuanto a forma, proporciones, tamaño, color...',
                    'Con el blanqueamiento dental conseguimos un color, una tonalidad y una luminosidad en la sonrisa óptimos.',
                    'Con la ortodoncia conseguiremos colocar los dientes en su posición adecuada mejorando no sólo la estética de los mismos, sino también los problemas funcionales.',
                    'Con las intervenciones quirúrgicas en las encías, gingivectomías, podemos retocar su contorno para conseguir una uniformidad y unas proporciones adecuadas.',
                ),
            ),
            'implantology' => array(
                'title_slider' => 'implantología',
                'title' => 'implantología',
                'content' => array(
                    '<strong>¿Qué son los implantes?</strong>',
                    'Son sustitutos de las raíces de los dientes cuando éstos han sido extraídos. Son, en realidad, tornillos de un material especial biocompatible (titanio) que se roscan en el hueso para sujetar, posteriormente, el diente o la muela que falta.',
                    '<strong>¿Por qué tiene tanta importancia reponer los dientes perdidos?</strong>',
                    'La reposición de las piezas que faltan en la boca es vital para mantener una correcta salud bucodental. Probablemente, si a todo el mundo que le falta algún diente fuese consciente de las anomalías, problemas y trastornos que ello desencadena, seguro que cuidaría más su dentadura y valoraría más el hecho de no extraer con tanta alegría las piezas dentales.',
                    'Por ello la premisa fundamental es INTENTAR SIEMPRE NO EXTRAER NINGUNA PIEZA DENTAL. Hoy en día la Odontología moderna dispone de métodos y técnicas que pueden conservar cualquier diente siempre que la raíz esté sana.',
                    'La falta de una pieza dental supone el deterioro progresivo del resto de las piezas',
                    'Sin embargo, si ya falta esa muela en la boca, los trastornos que ello desencadena se van estableciendo a lo largo de los años. Y esto es, precisamente, el gran problema, ya que el paciente no es consciente de lo que está sucediendo en su boca y los trastornos se establecen perjudicialmente de forma lenta y silenciosa resumiéndose en los siguientes puntos:',
                    '1. Movimiento de los dientes que rodean el espacio que deja la muela extraída. Las muelas que contactaban con la muela ausente se van extruyendo y las adyacentes se van tumbando hacia el espacio vacío. Ello añade la pérdida funcional de más muelas y también maloclusiones y trastornos temporomandibulares.',
                    '2. También provoca reabsorciones y pérdida de retención ósea en las muelas vecinas y en el propio espacio que deja la muela donde el proceso alveolar se va atrofiando progresivamente.',
                    '3. Igualmente, el paciente comienza a masticar por el otro lado desencadenando una abrasión dental exagerada en la zona de carga así como una alteración unilateral de la articulación temporomandibular.',
                    '4. Cuando la falta de piezas es más numerosa, la patología se multiplica y, sobretodo, los trastornos temporomandibulares, ya que la articulación precisa el soporte de los dientes para estar sana.',
                    '<strong>¿Qué ventajas tienen los implantes sobre otro tipo de prótesis?</strong>',
                    '1 Se mantiene y conserva el volumen óseo. Si no hay algo dentro del hueso, éste se va atrofiando progresivamente.',
                    '2 No hay que tallar los dientes adyacentes para sujetar la prótesis',
                    '3 La duración y el pronóstico de la prótesis no va a depender de las muelas que la sujetan. El implante es independiente y su éxito no está sujeto a la salud futura de las piezas adyacentes.',
                    '4 La sensación propioceptiva es más fisiológica. El paciente siente con más naturalidad un implante que un puente fijo.',
                ),
            ),
            'orthodontics' => array(
                'title_slider' => 'ortodoncia',
                'title' => 'ortodoncia',
                'content' => array(
                    'La ortodoncia es la especialidad de la odontología que se ocupa del diagnóstico, prevención y tratamiento de las malposiciones dentarias y de las deformaciones de los maxilares.',
                    'La corrección de los dientes y de las bases óseas se realiza mediante una serie de aparatos que pueden ser fijos o removibles, dependiendo de cada caso. Estos aparatos de ortodoncia y las técnicas de trabajo han evolucionado mucho, de manera que hoy en día disponemos de técnicas estéticas que han hecho que los aparatos prácticamente no se aprecien.',
                    'En nuestra clínica los tratamientos de ortodoncia están dirigidos a pacientes de todas las edades y el tratamiento es realizado por profesionales especializados y en colaboración con diversos profesionales de otras áreas para realizar a cada paciente un tratamiento interdisciplinario. A los niños se aconseja llevarlos al ortodoncista antes de los 6 años para poder detectar posibles alteraciones dentales o esqueléticas importantes de manera precoz y poder corregirlas.',
                    'Los doctores realizaran un exhaustivo examen clínico y un estudio mediante la toma de una serie de registros como: modelos dentales, fotografías faciales e intraorales y radiografías que les permitirán realizar un adecuado y personalizado plan de tratamiento.',
                    '<strong>La ortodoncia invisible</strong>',
                    'Consiste en una serie de alineadores prácticamente invisibles y extraíbles que se cambian progresivamente por uno nuevo. Cada uno de los alineadores se fabrica específicamente para tus diente. Conforme vayas cambiando de alineador, tus dientes se irán moviendo, poco a poco, semanas tras semana, hasta que estén alineados y en la posición final prescrita por el odontólogo.',
                    'Es la mejor manera de transformar tu sonrisa sin alterar tu vida cotidiana. Es removible ( se pueden extraer para comer, beber, cepillarse los dientes, etc.), cómodo (no hay brackets ni alambres que puedan irritar tus encías o el interior de tu boca) y está hecho a medida.',
                    '<strong>Ortodoncia fija</strong>',
                    'La ortodoncia fija es aquella que no puede ser retirada por el paciente.',
                    'Comúnmente es conocida como brackets, refiriéndose a las piezas, generalmente metálicas, que sujetas a los dientes sostienen el arco que produce el desplazamiento dental.',
                    'En los últimos años se han producido grandes avances en el campo de la ortodoncia fija existiendo una amplia gama adaptada en función cada uno de los casos y de las necesidades estéticas del paciente.',
                    '<strong>Ortodoncia de colores</strong>',
                    'Pensada especialmente para los más pequeños de la casa para los que llevar aparato puede generar problemas estéticos.',
                    'Se trata de una ortodoncia fija metálica cuyas ligaduras se pueden personalizar con sus colores favoritos.',
                    'La mejor forma de evitar que rechacen llevar brackets haciendo que se convierta en un juego',
                    '<strong>Ortodoncia autoligable</strong>',
                    'La ortodoncia de autoligado elimina las ligaduras de sujeción del arco al bracket, facilitando la limpieza y evitando la retención de restos de alimentos.',
                    'Además, el diseño de los brackets puede ayudar a que los dientes consigan la alineación adecuada más rápidamente, acortando así el tiempo de tratamiento.',
                    'Si además te preocupa la estética, cuentas también con la Ortodoncia Autoligable Cerámica, brackets del color de tus dientes para que sonreír no suponga un reto.',
                    '<strong>Ortodoncia fija cerámica</strong>',
                    'La ortodoncia cerámica es un tipo de aparatología fija en la que los brackets son fabricados en material cerámico adaptándose a la tonalidad del propio diente.',
                    'Al ser colocados sobre las piezas dentales, los brackets apenas son visibles proporcionándonos una estética extraordinaria.',
                    '<strong>Ortodoncia removible</strong>',
                    'La ortodoncia removible va anclada a los dientes pudiéndose quitar sin ayuda del odontólogo.',
                    'Indicada en determinado tipo de casos como movimientos dentales limitados o para modificar el crecimiento, puede ser retirada para comer o para su limpieza.',
                    'En muchas ocasiones la aparatología removible es usada como apoyo a la fija después de un tratamiento con ortodoncia fija o brackets.',
                ),
            ),
            'periodontics' => array(
                'title_slider' => 'peridoncia',
                'title' => 'peridoncia',
                'content' => array(
                    '<strong>¿Qué son las enfermedades periodontales?</strong>',
                    'Las enfermedades periodontales son un conjunto de enfermedades que afectan a los tejidos (periodonto) que rodean y sujetan a los dientes en los maxilares.',
                    'Son enfermedades de naturaleza inflamatoria y de causa infecciosa (causadas por bacterias) que dependiendo de su grado de afectación las denominamos gingivitis, cuando el proceso inflamatorio solamente afecta al periodonto superficial (la encía) y no están afectados los tejidos que sujetan el diente. Cuando el proceso inflamatorio afecta a los tejidos periodontales profundos, se produce destrucción del hueso y ligamento que soportan y sujetan los dientes. A este grado de afectación lo denominamos periodontitis. Si la periodontitis no se trata, ésta evoluciona y destruye todo el soporte del diente y con ello se produce la pérdida del diente.',
                    '<strong>Periodontitis</strong>',
                    'Las enfermedades periodontales son las enfermedades más extendidas del ser humano, incluye la gingivitis que consiste en una infección situada debajo de la encía, generalmente indolora que inflama la encía, hace que sangre fácilmente y si no se trata en alglinos individuos progresa a periodontitis.',
                    'La periodontitis, antiguamente llamada piorrea, destruye el hueso y el ligamento que soporta los dientes, provocando movilidad y separación de los dientes y si no se trata, pérdida de los mismos. Las enfermedades periodontales se pueden prevenir o en su defecto tratar con gran éxito, pudiendo así conservar los dientes toda nuestra vida.',
                    '<strong>¿Cuáles son los signos de la enfermedad periodontal?</strong>',
                    'Estos procesos inflamatorios raramente cursan un dolor intenso, por lo que muchos pacientes los padecen sin ser conscientes de ello. Los primeros signos y síntomas que aparecen en las fases más tempranas de la enfermedad (gingivitis) son el enrojecimiento e hinchazón de la encía, sangrado de las encías al cepillarse los dientes o de un modo espontáneo. Cuando las enfermedades periodontales han evolucionado a periodontitis, suelen aparecer otros síntomas tales como:',
                    '1. Retracción de las encías, sensación de dientes más largos.',
                    '2. Movilidad de los dientes. ',
                    '3. Separación de los dientes. ',
                    '4. Aumento de la sensibilidad dentaria, sobre todo al frío. ',
                    '5. Sensación de quemazón y dolor de encías. ',
                    '6. Mal aliento. ',
                    '7. Aparición de abscesos y flemones en la encía.',
                    '<strong>¿Cuáles son las causas de las enfermedades periodontales?</strong>',
                    'Se trata de enfermedades de causa infecciosa bacteriana. Se debe a unas bacterias que todos tenemos en la boca, alrededor de los dientes, y que si no eliminamos correctamente, ganan acceso al espacio entre el diente y la encía y pueden colonizar este espacio creciendo en número y provocando la reacción inflamatoria y destructiva que define a estas enfermedades. Las bacterias por si mismas no son capaces de provocar las consecuencias de estas enfermedades, sino que necesitan de un individuo susceptible (predisposición genética) y un medio ambiente adecuado (factores como tabaco y stress son factores de riesgo muy importantes en la colonización de estas bacterias).',
                    '<strong>¿Son las enfermedades periodontales muy frecuentes?</strong>',
                    'La gingivitis es una de las enfermedades más frecuentes del ser humano, siendo muy frecuente en todas las edades ( en jóvenes 75% de 20 a 25 años ).',
                    'Una parte de las gingivitis, aún en ausencia de tratamiento, no progresan a la destrucción de los tejidos periodontales de soporte dentario ( periodontitis ). La periodontitis es muy rara en jóvenes y adolescentes (aproximadamente 1 caso/10.000). Sin embargo su frecuencia aumenta con la edad (alrededor del 10% a los 30-40 años y del 25-30% entre 50 y 60 años). Por lo tanto, es una enfermedad',
                    '<strong>¿Desde que aparecen estos signos hasta que empieza a destruirse el hueso pasa mucho tiempo?</strong>',
                    'Depende, unos pacientes evolucionan rápido y otros lentamente. Los de evolución rápida acostumbran a ser jóvenes y pueden perder sus dientes en 5 años. En otros casos la evolución puede ser más lenta, pero si no se tratan adecuadamente puede llegar a perder también sus dientes.',
                    '<strong>¿Este tipo de infección que provoca la enfermedad periodontal puede afectar al estado general del paciente?</strong>',
                    'Si el paciente está en buen estado de salud, normalmente no se afecta su estado general, por ser una infección localizada exclusivamente en la boca. ',
                    'Sin embargo, puede afectar muy negativamente en casos de pacientes con enfermedades sistémicas crónicas tales como diabetes, enfermedades cardíacas, circulatorias, etc. Igualmente, está demostrada una clara asociación entre periodontitis y riesgo de infarto de miocardio y riesgo de niños prematuros y de bajo peso en mujeres embarazadas. Estas infecciones también pueden representar un grave riesgo a pacientes inmunodeprimidos (pacientes con defensas bajas, tal como ocurre en pacientes en tratamiento de cáncer, pacientes con SIDA, etc.).',
                    '<strong>¿La periodontitis o piorrea se cura?</strong>',
                    'Sí. El objetivo del tratamiento es eliminar la infección que produce la enfermedad. Una vez controlada la infección, el proceso destructivo del hueso se detiene y de este modo se pueden conservar los dientes y los tejidos periodontales sanos. Dependiendo de la cantidad de hueso de soporte destruído, los dientes una vez tratados tendrán mejor o peor pronóstico desde un punto de vista funcional. Por ello es importante un tratamiento precoz. ',
                    '<strong>¿Ha cambiado actualmente el antiguo concepto de que era?</strong>',
                    'Totalmente, no solamente el ser humano ha prolongado mucho sus años de vida, sino que también ha mejorado la calidad de vida de las personas de mayor edad.',
                    'Uno de los factores más importantes de esta mejor calidad de vida es el mantenimiento de la dentición en buenas condiciones de salud y función. Para ello es clave una adecuada prevención y tratamiento de las enfermedades periodontales. Con ello se podrán sobrepasar los 70 años con relativa facilidad, no sólo en cuanto a salud general, sino también al mantenimiento de una buena dentición.',
                ),
            ),
            'prosthesis' => array(
                'title_slider' => 'prótesis',
                'title' => 'prótesis dental',
                'content' => array(
                    'Una prótesis dental es un elemento artificial destinado para restaurar la anatomía de una o varias piezas dentarias restaurando también la relación entre los maxilares, a la vez que devuelve la dimensión vertical, y repone los dientes.',
                    'Los objetivos más importantes de las prótesis dentales son: la funcionalidad, la retención, el soporte y la estabilidad.',
                    'Hay diferentes tipos de prótesis dentales:',
                    'Removibles: son aquellas que el paciente puede retirar de la boca y volver a colocar por sí mismo.',
                    'No removibles: son aquellas que bien por cementado o atornillado, sólo puede colocar y retirar el odontólogo.',
                    'Prótesis mixta: Constan de una parte que va fija en la boca y otra que el paciente puede retirar y colocar.',
                    'Prótesis sobre implantes o implantosoportadas: Son aquellas que están soportadas por implantes fijos. Pueden estar fijas a los mismos, o ser un dispositivo mixto que pueda retirarse de los implantes con facilidad.',
                    '<strong>Uso de la prótesis, cuidados e higiene:</strong>',
                    'Cada prótesis presentará unas determinadas indicaciones de las que deberá ser informado el paciente por el odontólogo. Según el tipo de prótesis el paciente deberá concienciarse en mayor o menor medida del mantenimiento de la misma, no obstante, son las prótesis removibles (aquellas que el paciente puede retirar de la boca y colocar nuevamente por sí mismo), las que necesitarán de una especial indicación para el usuario. Algunos consejos generales para este tipo de prótesis bien podrían ser:',
                    'Lavar la prótesis después de cada comida, usando un cepillo de dientes convencional o bien un cepillo especial para prótesis (que se puede adquirir en cualquier farmacia), aplicando jabón después aclarar muy bien con agua. El uso de pasta dentífrica para limpiar este tipo de prótesis está contraindicado ya que provoca la opacidad y deterioro de la capa externa de la prótesis eliminando con el tiempo su brillo original, promoviendo la porosidad del material, que en el futuro facilitará la agregación de bacterias a la misma y consigo la formación de placa bacteriana y sarro.',
                    'Es importante la higiene de la prótesis así como de la propia boca, principalmente por salud y por tanto por comodidad y bienestar, evitando además el deterioro de la prótesis, así como malos olores o la simple pérdida de las características estéticas de la misma. Por ello el odontólogo suele insistir al paciente en la necesidad de realizar esta operación, si no tras cada comida, al menos una vez al día.',
                    'Retirar la prótesis para dormir, puesto que es recomendable que los tejidos de la boca descansen de la presión a la que se ven sometidas, al menos unas horas diariamente.',
                    'Mientras se mantenga la prótesis fuera de la boca, se aconseja conservarla en agua, bien sola, o bien añadiéndole unas pastillas desinfectantes preparadas para ese menester. De este modo se evitan golpes y deformaciones, al mismo tiempo que se conservan mejor los materiales de los que están hechas las prótesis.',
                    'Cuando se mantenga la prótesis retirada de la boca, tal vez antes de dormir, es aconsejable realizar masajes en las encías, puesto que de este modo se mejorará el riego sanguíneo, lo que hará que la reabsorción de los tejidos sea al menos algo menor.',
                    'En coronas y puentes fijos, el cepillado deberá realizarse de igual modo que el de una dentadura natural, existiendo hilos dentales especialmente diseñados para este tipo de prótesis, siendo recomendables también los enjuagues bucales con clorhexidina previamente recetados por el odontólogo.',
                    '<strong>Revisiones e incidencias:</strong>',
                    'Siempre que aparezcan ulceraciones, dolor o inestabilidad de la prótesis, deberá acudir de inmediato a la consulta del dentista.',
                    'Se debe realizar una revisión cada seis meses para observar el estado de los dientes y mucosas, para detectar desgastes en los dientes y realizar las adaptaciones apropiadas para corregir los desajustes provocados por el paso del tiempo.',
                ),
            ),
            'dentistry' => array(
                'title_slider' => 'odontopediatría',
                'title' => 'odontopediatría',
                'content' => array(
                    'La Odontopediatría es la rama de la Odontología que se dedica, en forma especial, a la atención de pacientes infantiles y adolescentes. Una de las medidas más importantes para mantener la salud bucal, es la visita temprana y regular al odontopediatra, quien podrá diagnosticar cualquier alteración que se haya producido. En el caso de los niños, esto cobra aún mayor importancia, debido a que las patologías que afectan a los dientes temporales o de leche, tienen una progresión más rápida que en los adultos.',
                    'Es por eso que, aconsejamos que las primeras visitas al odontopediatra coincidan con la aparición de los primeros dientes de leche, alrededor de los 6 meses de edad. Desde este momento, el odontopediatra podrá controlar el desarrollo de la dentición y el estado de la piezas dentarias, identificar alguna patología o alteración que requiera una atención prematura, enseñar las debidas y correctas pautas de higiene, tanto para el bebé como para los padres, contribuir a adoptar patrones alimenticios saludables y comenzar a instaurar procedimientos preventivos para evitar la aparición de caries y sus consecuencias.',
                    'Además, la visita temprana al odontopediatra, permitirá realizar procedimientos sencillos y agradables para los niños, contribuyendo de esta manera a crear un ambiente de seguridad y confianza, que se traducirá en pacientes adultos con mejor salud bucal y mayor disposición a la atención odontológica.',
                ),
            ),
        ),
    ),
);

// loyalty content
$app['loyalty'] = array(
    'es' => array(
        'intro' => 'ES :: text intro loyalty section',
        'content' => array(
            'La tarjeta dentalmarcos es un producto que nos permite ofrece a nuestros pacientes precios más ajustados manteniendo la calidad tanto en el servicio ofrecido como en los materiales utilizados.',
            'Mediante la tarjeta dentalmarcos el paciente accederá a una series de prestaciones gratuitas y descuentos en el precio del resto de los tratamientos ofrecidos en nuestra clínica',
        ),
        'free_services' => array(
            'title' => 'gratuitas',
            'services' => array(
                'Higiene bucal (1 anual)',
                'Revisiones (3, 6, 12 meses)',
                'Radiografías intraorales',
                'Estudio ortodoncia',
                'Educación bucodental',
            ),
        ),
        'discount_services' => array(
            'title' => 'descuentos',
            'services' => array(
                'Implantes',
                'Ortodoncia',
                'Prótesis',
                'Endodoncia',
                'Raspado',
            ),
        ),
        'services' => array(
            array(
                'image' => 'service-tooth',
                'title' => 'title-1',
                'text' => 'ES :: dummy text',
            ),
            array(
                'image' => 'service-clean',
                'title' => 'title-2',
                'text' => 'ES :: dummy text',
            ),
            array(
                'image' => 'service-right',
                'title' => 'title-3',
                'text' => 'ES :: dummy text',
            ),
        ),
        'annuities' => array(
            array(
                'price' => 70,
                'color' => 'panel-red',
                'type' => 'ES :: titular',
            ),
            array(
                'price' => 90,
                'color' => 'panel-blue',
                'type' => 'ES :: titular + hijo mayor 12 y menor 25 años',
            ),
            array(
                'price' => 105,
                'color' => 'panel-green',
                'type' => 'ES :: titular + cónyuge',
            ),
            array(
                'price' => 105,
                'color' => 'panel-grey',
                'type' => 'ES :: titular + cónyuge + hijo menor 12 años',
            ),
            array(
                'price' => 130,
                'color' => 'panel-grey',
                'type' => 'ES :: titular + cónyuge + hijo mayor 12 y menor 25 años',
            ),
            array(
                'price' => 150,
                'color' => 'panel-grey',
                'type' => 'ES :: titular + cónyuge + 2 o más hijos mayores 12 y menores 25 años',
            ),
        ),
    ),
    'ca' => array(
        'intro' => 'CA :: text intro loyalty section',
        'content' => array(
            'La tarjeta dentalmarcos es un producto que nos permite ofrece a nuestros pacientes precios más ajustados manteniendo la calidad tanto en el servicio ofrecido como en los materiales utilizados.',
            'Mediante la tarjeta dentalmarcos el paciente accederá a una series de prestaciones gratuitas y descuentos en el precio del resto de los tratamientos ofrecidos en nuestra clínica',
        ),
        'free_services' => array(
            'title' => 'gratuitas',
            'services' => array(
                'Higiene bucal (1 anual)',
                'Revisiones (3, 6, 12 meses)',
                'Radiografías intraorales',
                'Estudio ortodoncia',
                'Educación bucodental',
            ),
        ),
        'discount_services' => array(
            'title' => 'descuentos',
            'services' => array(
                'Implantes',
                'Ortodoncia',
                'Prótesis',
                'Endodoncia',
                'Raspado',
            ),
        ),
        'services' => array(
            array(
                'image' => 'service-tooth',
                'title' => 'title-1',
                'text' => 'CA :: dummy text',
            ),
            array(
                'image' => 'service-clean',
                'title' => 'title-2',
                'text' => 'CA :: dummy text',
            ),
            array(
                'image' => 'service-right',
                'title' => 'title-3',
                'text' => 'CA :: dummy text',
            ),
        ),
        'annuities' => array(
            array(
                'price' => 70,
                'color' => 'panel-red',
                'type' => 'CA :: titular',
            ),
            array(
                'price' => 90,
                'color' => 'panel-blue',
                'type' => 'CA :: titular + hijo mayor 12 y menor 25 años',
            ),
            array(
                'price' => 105,
                'color' => 'panel-green',
                'type' => 'CA :: titular + cónyuge',
            ),
            array(
                'price' => 105,
                'color' => 'panel-grey',
                'type' => 'CA :: titular + cónyuge + hijo menor 12 años',
            ),
            array(
                'price' => 130,
                'color' => 'panel-grey',
                'type' => 'CA :: titular + cónyuge + hijo mayor 12 y menor 25 años',
            ),
            array(
                'price' => 150,
                'color' => 'panel-grey',
                'type' => 'CA :: titular + cónyuge + 2 o más hijos mayores 12 y menores 25 años',
            ),
        ),
    ),
);

// payment content
$app['payment'] = array(
    'es' => array(
        'intro' => 'ES :: text intro payment section',
        'payments' => array(
            array(
                'image' => 'fa-money',
                'title' => 'ES :: Pago en efectivo o tarjeta',
                'text' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima maxime quam architecto quo inventore harum ex magni, dicta impedit.'
            ),
            array(
                'image' => 'fa-calendar',
                'title' => 'ES :: Financiación a medida',
                'text' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima maxime quam architecto quo inventore harum ex magni, dicta impedit.'
            ),
        ),

    ),
    'ca' => array(
        'intro' => 'ES :: text intro payment section',
        'payments' => array(
            array(
                'image' => 'fa-money',
                'title' => 'CA :: Pago en efectivo o tarjeta',
                'text' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima maxime quam architecto quo inventore harum ex magni, dicta impedit.'
            ),
            array(
                'image' => 'fa-calendar',
                'title' => 'CA :: Financiación a medida',
                'text' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima maxime quam architecto quo inventore harum ex magni, dicta impedit.'
            ),
        ),

    ),
);

// gallery content
$app['gallery'] = array(
    'es' => array(
        'intro' => 'ES :: Text intro gallery section',
        'videos' => array(
            array(
                'link' => 'https://www.youtube.com/watch?v=0hRxpzYtcWc',
                'thumbnail' => 'https://img.youtube.com/vi/0hRxpzYtcWc/0.jpg',
            ),
        ),
    ),
    'ca' => array(
        'intro' => 'CA :: Text intro gallery section',
        'videos' => array(
            array(
                'link' => 'https://www.youtube.com/watch?v=0hRxpzYtcWc',
                'thumbnail' => 'https://img.youtube.com/vi/0hRxpzYtcWc/maxresdefault.jpg',
            ),
        ),
    ),
);

// contact content
$app['contact'] = array(
    'es' => array(
        'intro' => 'ES :: text intro contact',
        'days' => 'Lunes a Viernes',
        'time' => '9:00 a 13:30 - 16:30 a 20:00',
        'address' => 'Avda. Pau Clarís 16, Local 08760 Martorell',
        'phones' => array(
            array(
                'text' => '93.775.30.60',
                'number' => '937753060',
            ),
            array(
                'text' => '93.775.51.57',
                'number' => '937755157',
            )
        ),
    ),
    'ca' => array(
        'intro' => 'CA :: text intro contact',
        'days' => 'Dilluns a Divendres',
        'time' => '9:00 a 13:30 - 16:30 a 20:00',
        'address' => 'Avda. Pau Clarís 16, Local 08760 Martorell',
        'phones' => array(
            array(
                'text' => '93.775.30.60',
                'number' => '937753060',
            ),
            array(
                'text' => '93.775.51.57',
                'number' => '937755157',
            )
        ),
    ),
);