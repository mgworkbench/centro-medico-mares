<?php

// news content
$app['news'] = array(
    'es' => array(
        'intro' => 'ES :: Text intro news section',
        'news' => array(
            array(
                'title' => 'ES :: Title new',
                'text' => 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo.',
                'link' => '',
                'img' => 'fotolia_86929299.jpg'
            ),
            array(
                'title' => 'ES :: Title new',
                'text' => 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo.',
                'link' => 'http://www.marca.com',
                'img' => 'fotolia_117905090.jpg'
            ),
            array(
                'title' => 'ES :: Title new',
                'text' => 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo.',
                'link' => 'http://as.com',
                'img' => 'fotolia_129206349.jpg'
            ),
            array(
                'title' => 'ES :: Title new',
                'text' => 'No img No link - Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo.',
                'link' => '',
                'img' => ''
            ),
        ),
    ),
    'ca' => array(
        'intro' => 'CA :: Text intro news section',
        'news' => array(
            array(
                'title' => 'CA :: Title new',
                'text' => 'CA :: Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo.',
                'link' => '',
                'img' => 'fotolia_86929299.jpg'
            ),
            array(
                'title' => 'CA :: Title new',
                'text' => 'CA :: Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo.',
                'link' => 'http://www.marca.com',
                'img' => 'fotolia_117905090.jpg'
            ),
            array(
                'title' => 'CA :: Title new',
                'text' => 'CA :: Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo.',
                'link' => 'http://as.com',
                'img' => 'fotolia_129206349.jpg'
            ),
            array(
                'title' => 'CA :: Title new',
                'text' => 'No img No link - Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo.',
                'link' => '',
                'img' => ''
            ),
        ),
    ),
);