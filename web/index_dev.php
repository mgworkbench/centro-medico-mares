<?php
use Symfony\Component\Debug\Debug;
// echo "<pre>";print_r($_SERVER);echo "</pre>";
// This check prevents access to debug front controllers that are deployed by accident to production servers.
// Feel free to remove this, extend it, or make something more sophisticated.
if (isset($_SERVER['HTTP_CLIENT_IP'])
    || isset($_SERVER['HTTP_X_FORWARDED_FOR'])
    // || !in_array(@$_SERVER['SERVER_NAME'], array('stagemares.mgworkbench.eu', 'centromedicomares.dev', 'dentalmarcos.mgworkbench.eu'))
    // || !in_array(@$_SERVER['SERVER_ADDR'], array('217.160.254.146', '212.227.18.91'))
    || !in_array(@$_SERVER['REMOTE_ADDR'], array('127.0.0.1', '81.47.167.10', '85.50.78.220', '88.3.211.197', 'fe80::1', '::1'))
) {
    header('HTTP/1.0 403 Forbidden');
    exit('You are not allowed to access this file. Check '.basename(__FILE__).' for more information.');
}

require_once __DIR__.'/../vendor/autoload.php';

Debug::enable();

$app = require __DIR__.'/../src/app.php';
require __DIR__.'/../config/dev.php';
require __DIR__.'/../src/admin_controller.php';
require __DIR__ . '/../src/web_controller.php';
$app->run();