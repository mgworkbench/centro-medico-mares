/*!
 * Start Bootstrap - Agency Bootstrap Theme (http://startbootstrap.com)
 * Code licensed under the Apache License v2.0.
 * For details, see http://www.apache.org/licenses/LICENSE-2.0.
 */

// jQuery for page scrolling feature - requires jQuery Easing plugin
$(function() {
    $('a.page-scroll').bind('click', function(event) {
        event.preventDefault();

        var $anchor = $(this);

        // scroll to section clicked
        var section = $anchor.attr('href');
        var scrollSection = '';

        if( $(section).length > 0){
            scrollSection = $(section);
        }else{
            section = section.replace('#','.');
            scrollSection = $(section);
        }

        $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top
        }, 1500, 'easeInOutExpo');

        sessionStorage.setItem("section", section);
    });

});

// Bind swipebox
$(function() {
    // swipebox
    $('.swipebox').swipebox();
    $( '.swipebox-video' ).swipebox();
});



// Highlight the top nav as scrolling occurs
$('body').scrollspy({
    target: '.navbar-fixed-top'
});

// Closes the Responsive Menu on Menu Item Click
$('.navbar-collapse ul li a').click(function() {
    // comprovar que no estamos en el menú de idiomas
    if( !$(this).hasClass('dropdown-toggle') ){
        $('.navbar-toggle:visible').click();
    }
});

// Check style of menu on reload page
if (sessionStorage.getItem("is_reloaded")){ 
    var section = $('.navbar-default li.active a').attr('href');

    if(section != '#page-top'){
        if( !$('.navbar-default').hasClass('navbar-shrink') ){
            $('.navbar-default').addClass('navbar-shrink');
        }else{
            $('.navbar-default').removeClass('navbar-shrink');
        }
    }
}

sessionStorage.setItem("is_reloaded", true);


$(document).ready(function(){
    // --------------------------------------
    //    language scroll
    // --------------------------------------

    // Save current section active on load page
    var section = '';

    if (null == sessionStorage.getItem('section')) {
        section = $('#dentalmarcos-navbar li.active a').attr('href');
    } else {
        section = sessionStorage.getItem('section');
    }
    sessionStorage.setItem("section", section);

    // Scroll to section when language change
    if (section != '#page-top') {
        var scrollSection = '';

        if( $(section).length > 0){
            scrollSection = $(section);
        }else{
            section = section.replace('#','.');
            scrollSection = $(section);
        }

        var scrollPosition = 40 + scrollSection.offset().top;

        $('html, body').stop().animate({
            scrollTop: scrollPosition
        }, 1500, 'easeInOutExpo');
    }

    // scroll to section when language change
    $('a.lang-menu').on('click', function(event) {
        event.preventDefault();
        if (null == sessionStorage.getItem('section')) {
            section = $('#dentalmarcos-navbar li.active a').attr('href');
        } else if (sessionStorage.getItem('section') != $('#dentalmarcos-navbar li.active a').attr('href')) {
            section = $('#dentalmarcos-navbar li.active a').attr('href');
        } else {
            section = sessionStorage.getItem('section');
        }
        sessionStorage.setItem("section", section);

        window.location.href = $(this).attr('href');
    });

    // --------------------------------------
    //    carousels
    // --------------------------------------

    // Carousel header
    $('#thumbnail-preview-indicators').carousel({
        interval: false
    });

    // Carousel services
    $('#carousel-services').on('slid.bs.carousel', function () {
        // on move carousel, active the tabpanel
        $('#carousel-services').find('.active').find('a').click();
    });

    // --------------------------------------
    //    Team boxes
    // --------------------------------------

    // set same height(max of boxes) on every box
    var maxHeightTeam = Math.max.apply(null, $("div.team-member-info").map(function ()
    {
        return $(this).outerHeight();
    }).get());

    $("div.team-member-info").css('height', maxHeightTeam);

    // --------------------------------------
    //    load CV modal content by Ajax
    // --------------------------------------

    $('a.team-read-more').on('click', function(){
        $('#cv').attr('data-url', $(this).data('url'));
    });

    $('#cv').on('show.bs.modal', function (event) {
        var cv = $(this);

        // ajax to get CV info
        $.ajax({
            async: false,
            url: cv.attr('data-url'),
            method: 'GET',
            dataType: 'JSON',
            success: function(data) {
                cv.find('.cv-name').text(data.name);
                cv.find('.cv-text').text(data.text);
                cv.find('img').attr('src', '/img/centromares/team/'+data.img);

                if (data.education.length !== 0) {
                    cv.find('.cv-education-title').text(data.education_title);

                    cv.find('.cv-education-list').empty();
                    $.each(data.education, function( index, value ) {
                        cv.find('.cv-education-list').append($("<li>").text(value));
                    });
                }

                if (data.experience.length !== 0) {
                    cv.find('.cv-experience-title').text(data.experience_title);

                    cv.find('.cv-experience-list').empty();
                    $.each(data.experience, function( index, value ) {
                        cv.find('.cv-experience-list').append($("<li>").text(value));
                    });
                }
            }
        });
    });


    // --------------------------------------
    //    'read more' feature in services
    // --------------------------------------

    // check height of first tab-pane.active to add 'read-more' feature
    if (calculateHeightTabpaneServices() < 250) {
        $('div.services-read-more').addClass('hide');
    } else {
        $('div.services-read-more').removeClass('hide');
    }

    // 'read more' feature
    $('div.services-read-more a').on('click', function(){
        readMoreFeature();
    });

    // show new tab after click on nav tab
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        resetReadMoreFeature();
    });


    // --------------------------------------
    //    Loyalty boxes
    // --------------------------------------

    // set same height(max of boxes) on every box
    var maxHeight = Math.max.apply(null, $("div.panel-body").map(function ()
    {
        return $(this).outerHeight();
    }).get());

    $("div.panel-body").css('height', maxHeight);


    // --------------------------------------
    //    Validate contact form
    // --------------------------------------
    $.validator.addMethod('validEmail',
        function(value, elem){
            return /[_a-z0-9-]+(.[_a-z0-9-]+)*@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,4})/.test(value);
        }
    );

    $.validator.addMethod('validPhone',
        function(value, elem){
            // validar según formato: https://en.wikipedia.org/wiki/List_of_country_calling_codes
            // return /\+?\d{1,3}?[- .]?\(?(?:\d{2,3})\)?[- .]?\d\d\d[- .]?\d\d\d\d/.test(value);

            // validar según formato: +34 9XX XX XX XX 9XX XX XX XX 6XX XX XX XX 7XX-XX-XX-XX 6XXXXXXXX
            return  /^((\+?34([ \t|\-])?)?[9|6|7]((\d{1}([ \t|\-])?[0-9]{3})|(\d{2}([ \t|\-])?[0-9]{2}))([ \t|\-])?[0-9]{2}([ \t|\-])?[0-9]{2})$/.test(value);
        }
    );

    $('#contactForm').validate({
        debug: true,
        rules: {
            name: {
                required: true
            },
            email: {
                required: true,
                validEmail: true
            },
            phone: {
                required: true,
                validPhone: true
            },
            message: {
                required: true
            }
        },
        messages: {
            name: {
                required: $('#contactForm #name').attr('data-validation-required-message')
            },
            email: {
                required: $('#contactForm #email').attr('data-validation-required-message'),
                validEmail: $('#contactForm #email').attr('data-validation-valid-email')
            },
            phone: {
                required: $('#contactForm #phone').attr('data-validation-required-message'),
                validPhone: $('#contactForm #phone').attr('data-validation-valid-phone')
            },
            message: {
                required: $('#contactForm #message').attr('data-validation-required-message')
            }
        },
        errorClass: "text-danger",
        submitHandler: function(form) {

            // get values from FORM
            var name = $("input#name").val();
            var email = $("input#email").val();
            var phone = $("input#phone").val();
            var message = $("textarea#message").val();
            var language = $("input#language").val();

            var firstName = name; // For Success/Failure Message
            // Check for white space in name for Success/Fail message
            if (firstName.indexOf(' ') >= 0) {
                firstName = name.split(' ').slice(0, -1).join(' ');
            }

            var response = {'es':'es::OK', 'ca':'ca::OK'};
            alert(response[language]);

            $.ajax({
                url: "/contact_me",
                type: "POST",
                dataType: "JSON",
                data: {
                    name: name,
                    phone: phone,
                    email: email,
                    message: message
                },
                cache: false,
                success: function(data) {
                    if(data.status){
                        // Success message
                        $('#success').html("<div class='alert alert-success'>");
                        $('#success > .alert-success').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;").append("</button>");
                        $('#success > .alert-success').append("<strong>Your message has been sent. </strong>");
                        $('#success > .alert-success').append('</div>');
                    }else{
                        // Fail message
                        $('#success').html("<div class='alert alert-danger'>");
                        $('#success > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;").append("</button>");
                        $('#success > .alert-danger').append("<strong>WTF! Sorry " + firstName + ", it seems that my mail server is not responding. Please try again later!");
                        $('#success > .alert-danger').append('</div>');
                    }

                    //clear all fields
                    $('#contactForm').trigger("reset");
                },
                error: function() {
                    // Fail message
                    $('#success').html("<div class='alert alert-danger'>");
                    $('#success > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;").append("</button>");
                    $('#success > .alert-danger').append("<strong>Sorry " + firstName + ", it seems that my mail server is not responding. Please try again later!");
                    $('#success > .alert-danger').append('</div>');
                    //clear all fields
                    $('#contactForm').trigger("reset");
                }
            });
        }
    });
});


// calculate height of current tab-pane active
function calculateHeightTabpaneServices()
{
    var totalTabpaneActiveHeight = 0;

    $("div.tab-pane.active").children().each(function(){
        totalTabpaneActiveHeight = totalTabpaneActiveHeight + $(this).outerHeight();
    });

    return totalTabpaneActiveHeight;
}

// reset 'read more' feature
function resetReadMoreFeature()
{
    // show/hide button to 'read more'
    if (calculateHeightTabpaneServices() < 250) {
        $('div.services-read-more').addClass('hide');
    } else {
        $('div.services-read-more').removeClass('hide');
    }

    // reset text of button
    var text = $('div.services-read-more a').data('more');

    $('div.services-read-more a').text(text);
    $('div.services-read-more a').addClass('more');

    // reset css properties of elements
    $('div.tab-pane.active').css('height', '270px');
    $('div.tab-content').css('height', '400px');
    $('div.services-read-more a').parent().css('position', 'absolute');
}

// 'read more' feature
function readMoreFeature()
{
    var text = '';

    if ($('div.services-read-more a').hasClass('more')) {
        text = $('div.services-read-more a').data('less');

        // expand tab-pane with animation
        $('div.tab-pane.active').css('height', 'auto');
        $('div.tab-content').css('height', 'auto');

        $('div.services-read-more a').parent().css('position', 'static');
        $('div.services-read-more a').text(text);
        $('div.services-read-more a').removeClass('more');
    } else {
        text = $('div.services-read-more a').data('more');

        // collapse tab-pane with animation
        $('html, body').animate({
            scrollTop: $('div.board').offset().top - $('nav').outerHeight()
        }, 1500, function(){
            $('div.tab-pane.active').css('height', '270px');
            $('div.tab-content').css('height', '400px');

            $('div.services-read-more a').parent().css('position', 'absolute');
            $('div.services-read-more a').text(text);
            $('div.services-read-more a').addClass('more');
        });

    }
}
