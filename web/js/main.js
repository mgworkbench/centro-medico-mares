/*
 * Custom JQuery functions
 */

$(document).ready(function(){
    $('.carousel-caption a').on('click',function(event){
        var scrollSection = $(this).attr('href');
        
        $('html, body').stop().animate({
            //scrollTop: $($anchor.attr('href')).offset().top
            scrollTop: $(scrollSection).offset().top
        }, 1500, 'easeInOutExpo');
        event.preventDefault();
    })

});


/* alternate section: services */
$(function(){
    $('a[title]').tooltip();
});

// carousel services
$('#carousel-services').on('slid.bs.carousel', function () {
    // on move carousel, active the tabpanel
    $('#carousel-services').find('.active').find('a').click();
})

