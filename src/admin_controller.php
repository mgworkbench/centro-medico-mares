<?php

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

//require_once __DIR__.'/../config/content.php';
//require_once __DIR__.'/../config/content_news.php';

//Request::setTrustedProxies(array('127.0.0.1'));

$app->before(function (Request $request) use ($app) {
    $app['twig']->addGlobal('request_uri', $request->getRequestUri());
    $app['twig']->addGlobal('route', $request->get("_route"));
});

// Admin controller
$app->get('/login', function(Request $request) use ($app) {
    return $app['twig']->render('login.html', array(
        'error'         => $app['security.last_error']($request),
        'last_username' => $app['session']->get('_security.last_username'),
    ));
})
->bind('login');

$app->get('/admin', function(Request $request) use ($app) {
    $token = $app['security.token_storage']->getToken();

    if (null === $token) {
        return $app->redirect('login');
    }

    return $app['twig']->render('admin/index.html', array());
})
->bind('admin');

$app->match('admin/config', function(Request $request) use ($app) {
    // load the file
    $contents = file_get_contents(__DIR__.'/../config/config.json');

    // decode json data into a php array
        $contentsDecoded = json_decode($contents, true);

    return $app['twig']->render('admin/config.html', array('config' => $contentsDecoded));
})
->bind('admin_config');

$app->match('admin/news', function (Request $request) use ($app) {

    // load json file
    $content = file_get_contents(__DIR__ . '/../content/news.json');

    if (false === $content)
        return $app['twig']->render('errors/admin.html', array('message' => 'JSON file not found'));

    // decode json data into a php array
    $contentsDecoded = json_decode($content, true);

    if (true == $request->isMethod('POST')) {
        // get data edited
        $contentsDecoded = $request->request->all();

        // encode php array back into json string
        $json = json_encode($contentsDecoded);

        // save the file
        file_put_contents(__DIR__ . '/../content/news.json', $json);

        $app['session']->getFlashBag()->add('info', ['alert-success' => 'Data saved successfully']);

        return $app->redirect('news');
    }

    return $app['twig']->render('admin/news.html', array('news' => $contentsDecoded));
})
->bind('admin_news');

