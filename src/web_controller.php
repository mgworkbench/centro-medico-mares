<?php

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

require_once __DIR__.'/../config/content.php';
require_once __DIR__.'/../config/content_news.php';

//Request::setTrustedProxies(array('127.0.0.1'));

// Form controller
$app->post('/contact_me', function (Request $request) use ($app) {
    // echo "<pre>";
    // print_r($request->get('email'));
    // echo "</pre>";
    // die;
    
    // $res = json_encode(array('status'=>true,'message'=>'Message send correctly'));
    // return new Response($res , 200 , array( 'Content-Type' => 'application/json' ));
    $from_name = $request->get('name');
    $from_mail = $request->get('email');
    $subject = "Info Dental Marcos";
    $content = $request->get('message');

    $from = $from_name.' <'.$from_mail.'>';

    // $message = \Swift_Message::newInstance()
    //     ->setSubject('[YourSite] Feedback')
    //     ->setFrom(array('noreply@yoursite.com'))
    //     ->setTo(array($email))
    //     ->setBody($request->get('message'));
 
    // $r = $app['mailer']->send($message); 

    // return json_encode(array('status'=>false, 'message'=>'Message send correctly', 'r'=>$r));

    $to = 'mginov@gmail.com';

    $headers   = array();
    $headers[] = 'MIME-Version: 1.0';
    $headers[] = 'Content-type: text/plain; charset=iso-8859-1';
    $headers[] = 'From: '.$from;
    $headers[] = 'Subject: '.$subject;

    $send_contact = mail($to, $subject, $content, implode("\r\n", $headers));

    return new JsonResponse(array('status' => $send_contact));
});

$app->get('/cv/{_locale}/{id}', function ($_locale, $id) use ($app) {

    $data = (isset($app['team'][$_locale]['employees'][$id-1])) ? $app['team'][$_locale]['employees'][$id-1] : false;

    if ($data) {
        $cv = array(
            'img'   =>  $data['img'],
            'name'  =>  $data['name'],
            'text'  =>  $data['cv']['text'],
            'education_title'   =>  $app['translator']->trans('team-section.education-title'),
            'education'  =>  $data['cv']['education'],
            'experience_title'  =>  $app['translator']->trans('team-section.experience-title'),
            'experience'  =>  $data['cv']['experience'],
        );

        return new JsonResponse($cv);
    }

    return new JsonResponse(array());
})
->value('_locale', 'es')
->value('id', -1)
->bind('cv');


// Base controllers
$app->get('/{_locale}/{section}', function ($section) use ($app) {

    // abort when page not fount
    if (!in_array($app['translator']->getLocale(), array('es','ca'))) {
        $app->abort(404, "Page not found.");
    }

    // get dynamic content in language request
    $lang = $app['translator']->getLocale();
    $content_web = array();

    $content_web['header'] = $app['header'][$lang];

    $content_web['sections'] = array(
        'team'      =>  $app['team'][$lang],
        'services'  =>  $app['services'][$lang],
        'loyalty'   =>  $app['loyalty'][$lang],
        'payment'   =>  $app['payment'][$lang],
        'gallery'   =>  $app['gallery'][$lang],
        'news'      =>  $app['news'][$lang],
        'contact'   =>  $app['contact'][$lang],
    );
//dump($content_web['sections']['news']);die;
    // Testing services
    //echo $app['some_service'];die;

    // Testing dinamic gallery
    /*
    $galleryFinder = new Symfony\Component\Finder\Finder();
    $gallery_directory = __DIR__.'/../web/img/gallery';

    $images = array();
    if (is_dir($gallery_directory)) {
        $galleryFinder->files()->in($gallery_directory)->name('big-*');
        foreach($galleryFinder as $file) {
            dump($file->getFileName());
            $i = explode('-', $file->getFileName());dump($i);
            $ii = str_replace('big-','small-',$file->getFileName());dump('d'.$ii);
            array_push($images, $file->getFileName());
            $galleryFinder = $galleryFinder->create();
            $galleryFinder->files()->in($gallery_directory)->name('d'.$ii);
            foreach($galleryFinder as $file) {
                dump($file);
            }

            die;
        }
        $galleryFinder = $galleryFinder->create();
        $galleryFinder->files()->in($gallery_directory)->name('small-*');
        foreach($galleryFinder as $file) {
            //dump($file->getFileName());
        }
    }

    dump($images);
    die;
    */

    return $app['twig']->render('index.html', array('section'=>$section, 'content'=>$content_web));
})
->value('_locale', 'es')
->value('section', false)
->bind('homepage')
;


$app->error(function (\Exception $e, Request $request, $code) use ($app) {
    if ($app['debug']) {
        return;
    }


// get dynamic content in language request
    $lang = $app['translator']->getLocale();
    $content_web = array();

//    $content_web['header'] = $app['header'][$lang];

//    switch ($code) {
//        case 404:
//            return $app['twig']->render('errors/404.html');
//            $message = 'The requested page could not be found.';
//            break;
//        default:
//            $message = 'We are sorry, but something went terribly wrong.';
//    }
//
//    return new Response($message);

    // 404.html, or 40x.html, or 4xx.html, or error.html
    $templates = array(
        'errors/'.$code.'.html',
        'errors/'.substr($code, 0, 2).'x.html',
        'errors/'.substr($code, 0, 1).'xx.html',
        'errors/default.html',
    );
//dump($templates);die;
    return new Response($app['twig']->resolveTemplate($templates)->render(array('code' => $code)), $code);
});

