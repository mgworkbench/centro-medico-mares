<?php

use Silex\Application;
use Silex\Provider\TwigServiceProvider;
use Silex\Provider\RoutingServiceProvider;
use Silex\Provider\ValidatorServiceProvider;
use Silex\Provider\ServiceControllerServiceProvider;
use Silex\Provider\HttpFragmentServiceProvider;
use Symfony\Component\Translation\Loader\YamlFileLoader;

// add default timezone to avoid error with debug profiler in 1and1
date_default_timezone_set("Europe/Madrid");

$app = new Application();
$app->register(new RoutingServiceProvider());
$app->register(new ValidatorServiceProvider());
$app->register(new ServiceControllerServiceProvider());
$app->register(new TwigServiceProvider());
$app->register(new HttpFragmentServiceProvider());
$app->register(new Silex\Provider\SwiftmailerServiceProvider());

// add twig feature
$app['twig'] = $app->extend('twig', function ($twig, $app) {
    // add custom globals, filters, tags, ...

    $twig->addFunction(new \Twig_SimpleFunction('asset', function ($asset) use ($app) {
        return $app['request_stack']->getMasterRequest()->getBasepath().'/'.$asset;
    }));

    return $twig;
});

// add translation feature
$app->register(new Silex\Provider\TranslationServiceProvider(), array());
$app->register(new Silex\Provider\LocaleServiceProvider(), array());

$app['translator'] = $app->extend('translator', function($translator, $app) {
    $translator->addLoader('yaml', new YamlFileLoader());
 
    $translator->addResource('yaml', __DIR__.'/../config/locales/es.yml', 'es');
    $translator->addResource('yaml', __DIR__.'/../config/locales/ca.yml', 'ca');
 
    return $translator;
});

$app['translator']->setLocale('es');
$app['translator']->setFallbackLocale('es');

// add swiftmailer feature
$app['swiftmailer.options'] = array(
    'host'       => 'smtp.gmail.com',
    'port'       => 465,
    'username'   => 'mginov@gmail.com',
    'password'   => 'travonig1483921',
    'encryption' => 'ssl',
    'auth_mode'  => 'login'
);

// add session feature
$app->register(new Silex\Provider\SessionServiceProvider());

// add security
$app->register(new Silex\Provider\SecurityServiceProvider());

$app['security.encoder.bcrypt.cost'] = 12;

$app['security.firewalls'] = array(
    'admin' => array(
        'pattern' => '^/admin',
        'form' => array(
            'login_path' => '/login',
            'check_path' => '/admin/login_check',
        ),
        'users' => array(
            'admin' => array(
                'ROLE_ADMIN',
                '$2a$12$jMRu3TLF58qDIihDH9qwv.ZKBK7dIGsXQTLJdZOwZdpd8XiQ7ucCW',
            ),
        ),
        'logout' => array(
            'logout_path' => '/admin/logout',
            'invalidate_session' => true,
        ),
    ),
);

return $app;
